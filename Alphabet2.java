// m�thode 2 : if... else...

public class Alphabet2 {
	public static void main (String[] args) {
		char[] myAlphabet = {'a','A','b','B','c','C'};
	
		for (int i=0; i<myAlphabet.length; i++) {
			
			if (Character.isUpperCase(myAlphabet[i])) {
			System.out.println(myAlphabet[i] + " is uppercase");
			}
			else {
		    System.out.println(myAlphabet[i] + " is lowercase");
		    }
	}

	}
}
