public class Chien {
	
	String nom;
	String race;
	String abboiement;
	String couleur;
	
	public Chien () {
		nom = "Inconnu";
		race = "Inconnue";
		abboiement = "Inconnu";
		couleur = "Inconnue";
	}
		
	public Chien (String pNom, String pRace, String pAbboiement, String pCouleur) {
		nom = pNom;
		race = pRace;
		abboiement = pAbboiement;
		couleur = pCouleur;
	}
	
	public static void main (String[] args) {
		Chien c1 = new Chien ("M�dor", "Labrador", "Ouaf", "Beige");
		Chien c2 = new Chien ("Toutou", "Rottweiler", "Wouf", "Noir");
		System.out.println(c1.race);
		System.out.println(c2.race);
	}

	public String getNom() {
		return nom;
	}
	
	public String getRace() {
		return race;
	}
	
	public String getAbboiement() {
		return abboiement;
	}
	
	public String getCouleur() {
		return couleur;
	}
	
	System.out.println(c1 = "c1.geNom()");
}