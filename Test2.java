import java.util.Arrays;

public class Test {
	
	// cr�er un caract�re et v�rifier s'il est en minuscule ou majuscule
	public static void main(String[] args) {
	char maVar = ('a');
		System.out.println(Character.isLowerCase(maVar));
		System.out.println(Character.isLowerCase('D'));
		
	// 	v�rifier si plusieurs caract�res sont en minuscule
	char [] maVar1 = {'a', 'B', 'c', 'D', 'E'};
	for (int i=0; i<maVar1.length; i++)
		System.out.println(Character.isLowerCase(maVar1[i]));
	for (char lettre : maVar1)
		System.out.println(Character.isLowerCase(lettre));
	
	// transformer des caract�res majuscules en minuscules
	char [] maVar2 = {'a', 'B', 'c', 'D', 'E'};
	for (int i=0; i<maVar2.length; i++)
		System.out.println(Character.toLowerCase(maVar2[i]));
	
	// transformer une chaine de caract�res (string) en tableau puis v�rifier s'ils sont en minuscules ou majuscules
	String maStr1 = "aBcDE";
	char [] maVar3 = maStr1.toCharArray();
	for (int i=0; i<maVar3.length; i++)
		System.out.println(Character.isLowerCase(maVar3[i]));
	
	
	char [] maVar4 = {'a', 'B', 'c', 'D', 'E'};
	for (int i=0; i<maVar4.length; i++)
	if (Character.isLowerCase(maVar4[i] = true))
		System.out.println("Minuscule");
	
	else 
		System.out.println("Majuscule");
		
	
	}
}
