public class ChienSeb {
	int chienAge;
	
	public ChienSeb(String name) {
		System.out.println("Name chosen is :" + name);
	}
	
	public void setAge( int age ) {
		chienAge = age;
	}
	
	public int getAge1( ) {
		System.out.println("Chien's age is :" + chienAge );
		return chienAge;
	}
	
	public int getAge( ) {
		System.out.println("Chien's age is :" + chienAge );
		return chienAge;	
	}
	
	public static void main(String []args) {
		ChienSeb monChien = new ChienSeb( "Tommy" );
		monChien.setAge( 7 );
		monChien.getAge1( );
		System.out.println("Variable Value :" + monChien.chienAge );
	}
}
