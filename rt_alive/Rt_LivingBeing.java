package rt_alive;

public interface Rt_LivingBeing {
	public String breathing();
	public void eating();
	public void drinking();
	public void dying();
}
